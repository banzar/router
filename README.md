# Router

Solve vehicle routing problems in an intuitive way.

## Installation

Install project dependencies using the package manager:

```bash
npm install
```

## Usage

Run the project locally using the development server:

```bash
npm start
```

## Contributing

See the [Contributor Covenant Code of Conduct](CONTRIBUTING.md).

## License

See the [GNU General Public License](LICENSE.md).
